# MaxSat Benchmarks Code Base

Code for managing MaxSat instances submitted to the MaxSat Evaluations.

## Building

1. For best results use a recent version of CMAKE (>= 3.13).
2. Change to downloaded `maxsat_benchmarks_code_base` dir.
3. `mkdir build`
4. `cmake -S . -B build`
5. `cmake --build build`
6. binaries will appear in `bin`.

## Available Programs
- [std_wcnf](#markdown-header-std_wcnf):&nbsp; &nbsp; &nbsp; convert MaxSat instance to standard form.
- [verify_soln](#markdown_header-verify_soln):&nbsp; check the solutions output by a MaxSat solver.
- [to_cnf](#markdown_header-to_cnf):&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;convert wcnf file to cnf (for input to Sat solver.
- [to_old_fmt](#markdown_header-to_old_fmt):&nbsp;convert new format wcnf file to old format (with p-line)

## std_wcnf
`std_wcnf [-h|-help|--help] [-old] [-top_exact] [-preserve] [--] [wcnf_file]`

<pre>
Input a MaxSat instance from "wcnf_file" or "&lt;stdin>" and output
a <b>standardized</b> MaxSat instance to &lt;stdout>.
</pre>

### Input
A MaxSat instance in any of the formats used in the MaxSat
Evaluations. Input file can be gzipped (but output will not be).

**Note**: each
clause in input instance must start and end on a new line (multi-line 
clauses are not allowed)

### Output	
A **standarized** MaxSat instance conforming to the requirements of
the MaxSat Evaluation.

### Options
<pre>
-old	   output in old MaxSat format with a p-line and top equal to the
		   sum of the soft clause wts + 1. <b>Default:</b> output in
		   the new format where soft clauses all have a weight
		   and hard clauses have an "h " prefix.
-top_exact For inputs in the old format with a p-line, only clauses
		   with weight == top are treated as being hard. <b>Default</b>
		   clauses with weight &geq; top are treated as hard. 
-preserve  Do not modify mono-weighted instances to have soft weights of 1, and
		   do not remap the variables to remove gaps.
--		   regard following argument to be the name of the wcnf-file
		   useful for unsually named files (like those staring with '-')
</pre>

An initial set of comment lines are added to the top of the
standardized instance.	These lines once the initial **c** character
is striped form a **json** record containing information about the
file. The format of these initial comments is:

```
c Standarized MaxSat Instance
c{
c sha1sum: <sha1 code of instance with all comment lines removed>,
c nvars:   <number of variables>,
c ncls:	   <number of clauses>,
c total_lits: <total number of literals in all clauses>,
c nhards:  <number of hard clauses>
c nhard_nlits: <total number of literals in the hard clauses>
c nhard_len_stats:
c	 { min:		<minimum length hard clause>,
c	   max:		<maximum length hard clause>,
c	   ave:		<average length hard clauses>,
c	   stddev:	<standard deviation of hard clause lengths>},
c nsoft_len_stats:
c	 { min:		<minimum length soft clause>,
c	   max:		<maximum length soft clause>,
c	   ave:		<average length soft clauses>,
c	   stddev:	<standard deviation of soft clause lengths>},
c nsoft_wts: <number of distinct soft clause weights>,
c soft_wt_stats:
c	 { min:		<minimum soft clause weight>,
c	   max:		<maximum soft clause weight>,
d	   ave:		<average soft clauses weight>,
c	   stddev:	<standard deviation of soft clause weights>}
c}
c------------------------------------------------------------;
```
### Return Code 
Return code of 1 is any errors encountered during conversion to standard form.

## verify_soln
`verify_soln [-h|-help|--help|-help-topk] [-mlv] [-v] [-e] [-s] [-topk] [-o <OPTMIMALWT>] wcnf_file [soln_file]`

<pre>
Read a MaxSat instance from <b>wcnf_file</b> and a solver's output
from <b>soln_file</b> (or &lt;stdin> if <b>soln_file</b> is not specified),
verify all solutions in solver's output 
</pre>

### Input
`wcnf_file` must be a **standardized** MaxSat instances. `soln_file`
consists of pairs of o-lines and v-lines. Each v-line matches the
previous o-line. Unmatched o-lines are ignored. The v-line must
specify a model for the MaxSat instance that (a) satisfies all hard
clauses and (b) achieves cost equal to the previous matching
o-line. The input can also contain at an `s OPTIMAL` line if the best
solution returned is claimed to be optimal. With -o a known optimum
value can be supplied and this supplied value will be used to check if
the v-line does in fact achieve the optimal .

### Output
For each o-line/v-line pair the line
<pre>
c <WT> [VERIFIED|INVALID] [NOT_MATCHED]
</pre>
is printed to &lt;stdout>, where &lt;WT> is the cost of the
v-line solution.

- `VERIFIED` is printed if the v-line solution satisfies the hard clauses

- `INVALID` is printed if the v-line solution does not satisfy the hard clauses

- `VERIFIED NOT_MATCHED` is printed if the v-line solution is
`VERIFIED` but does not have cost matching the previous o-line.

If an `s OPTIMAL` is found in the solver input then the line
<pre>
c &lt;BESTWT> CLAIMED OPTIMAL
</pre>
is output, where `<BESTWT>` is the best valid solution found
in the input.

If a specified optimal is passed with an -o parameter then
<pre>
c &lt;BESTWT> VALID OPTIMAL
</pre> is output if the best solution achieves the specified
optimal.

### Options
<pre>
-mlv   allow v-lines split over multiple lines
	   (consecutive v-lines will be merged!)
-v	   Output the v-lines.
-e	   Echo all soln-file lines.
-s	   is given the solution file is expected to be in STAREXEC format,
	   where each line is tagged by an initial time stamp.
-topk  verify output from topk track (use <b>-help-topk</b> for more info)	 
-o &lt;OPTIMALWT>
	   A known optimal cost can be passed with the `-o <OPTIMALWT>` option.
	   Any <b>s OPTIMAL</b> line in the solver's output can then be verified
	   against the passed `<OPTIMALWT>`. 
</pre>

### Return Code 
Return code is 0 if all v-lines are verified, 1 if any v-line is
invalid or if an `s OPTIMAL` line is specified and the best solution
is not as good as the passed `-o <OPTIMALWT>` parameter.

## to_cnf
`to_cnf [-h|-help|--help] [-hards] [--] [wcnf_file]`
<pre>
Input a MaxSat instance from "wcnf_file" or "&lt;stdin>", convert all clauses (hard and
softs) into ordinary clauses and output the result as a DIMACS standard CNF file to
&lt;stdout>.
</pre>

### Input
A MaxSat instance in any of the formats used in the MaxSat Evaluations. Input files can be gzipped. If input file is not specified read from &lt;stdin>

### Output
A Sat instance in standard DIMACS format output to &lt;stdout>

### Options
<pre>
-hards   Only output the hard clauses (allows checking if any feasible solutions exist).
--       regard following argument to be the name of the wcnf-file 
</pre>

### Return Code
Return 1 if any errors occur when parsing the wcnf-file, return 0 otherwise.

## to_old_fmt
`to_old_fmt [-h|-help|--help] [wcnf_file]`
<pre>
Input a MaxSat instance from "wcnf_file" or "&lt;stdin>" and output an old format
(with p-line) wcnf instance to &lt;stdout>.
</pre>

### Input
A MaxSat instance in any of the formats used in the MaxSat Evaluations. Input files can be gzipped. If input file is not specified read from &lt;stdin>

### Output
A MaxSat instance in old format (with p-line) output to &lt;stdout>

### Return Code
Return 1 if any errors occur when parsing the wcnf-file, return 0 otherwise.

## Other Software Used

- `contrib/iostreams3` from [https://github.com/madler/zlib.git](https://github.com/madler/zlib.git) by Ludwig Schwardt
   to provide an `std::istream` to gzipped files. 