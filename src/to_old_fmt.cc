/***********[to_old_fmt.cc]
Copyright (c) 2020, Fahiem Bacchus

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

***********/
#include <fstream>
#include <iostream>
#include <stdexcept>
#include "zfstream.h"

#include "Wcnf.h"

using std::cerr;
using std::cin;
using std::cout;
using std::runtime_error;
using std::string;

void print_usage(const char* name) {
  // clang-format off
  cerr << "USAGE: " << name
       << " [-h|-help|--help] [wcnf_file] >old_fmt_wcnf_file\n"
       << "    reads instance from wcnf_file and outputs the instance in the\n"
       << "    old format (i.e., with a p-line)\n"
       << "    no wcnf_file argument ==> read from stdin.\n";
  // clang-format on
}

int main(int argc, char* argv[]) {
  /*
    Operate as filter converting a maxsat wcnf instance into an old
    format wcnf instance (with a p-line). Input wcnf can be any
    format. Although the program is most useful for converting new
    format with hard clauses marked with an 'h ' prefix to p-line
    instances it can also be used to convert ancient format instances
    (cnf instances and no top instances) into the most recent p-line
    format.
   */
  std::ios::sync_with_stdio(false);
  cin.tie(0);
  cout.tie(0);
  cerr.tie(0);

  string file_name;
  try {
    for (int i = 1; i < argc; ++i) {
      string arg;
      arg = argv[i];
      if (arg == "-help" || arg == "-h" || arg == "--help") {
        print_usage(argv[0]);
        exit(1);
      }
      else {
        if (!file_name.empty())
          throw runtime_error("extranous argument \"" + arg + "\"\n");
        file_name = arg;
      }
    }
  } catch (std::runtime_error& ex) {
    cerr << "Error: bad arguments " << ex.what();
    print_usage(argv[0]);
    exit(1);
  } catch (...) {
    print_usage(argv[0]);
    exit(1);
  }

  gzifstream ifs;
  if (!file_name.empty()) {
    ifs.open(file_name, std::ios::in | std::ios::binary);
    if (!ifs.is_open()) {
      cerr << "Error: could not open file \"" << file_name << "\"\n";
      exit(1);
    }
  }
  try {
    Wcnf theFormula;
    theFormula.set_old_fmt(true);
    theFormula.set_preserve(true);
    theFormula.loadFromStream(file_name.empty() ? cin : ifs);
    if (!file_name.empty()) ifs.close();
    theFormula.compute_pline();
    theFormula.outputInstance(cout);
  } catch (std::runtime_error& ex) {
    cerr << "Error: " << ex.what();
    return 1;
  } catch (...) {
    cerr << "Error: unknown";
    return 1;
  }
  return 0;
}
